package thread.win7;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import common.Static;
import sys.CMDService;
import ui.Win7OemActivatieJPanel;
import util.MyFile;

public class Win7OemActivatie implements Runnable {

	private String lic;
	private String key;
	private String cosBootPath;

	public Win7OemActivatie(String lic, String key, String cosBootPath) {
		this.lic = lic;
		this.key = key;
		this.cosBootPath = cosBootPath;
	}

	@Override
	public void run() {

		Win7OemActivatieJPanel.getIstance().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Win7OemActivatieJPanel.getIstance().runActivatieButton.setEnabled(false);
		Win7OemActivatieJPanel.getIstance().textArea.setText("");
		Win7OemActivatieJPanel.getIstance().progressBar.setValue(0);

		// 复制相关文件至系统temp目录下
		boolean isTempFileExist = false;
		String licTmpPath = null;
		String grldrTmpPath = null;
		String bootinstTmpPath = null;
		String bootrestTmpPath = null;
		File licFile = null;
		File grldrFile = null;
		File bootinstFile = null;
		File bootrestFile = null;
		try {
			licTmpPath = URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "lic.xrm-ms";
			grldrTmpPath = URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "grldr";
			bootinstTmpPath = URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "bootinst.exe";
			bootrestTmpPath = URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "bootrest.exe";
			MyFile.inputStreamToFile(
					Win7OemActivatie.class.getResourceAsStream("/resource/win7/lic/" + lic + "-lic.xrm-ms"),
					licTmpPath);
			MyFile.inputStreamToFile(Win7OemActivatie.class.getResourceAsStream("/resource/win7/slic/grldr"),
					grldrTmpPath);
			MyFile.inputStreamToFile(Win7OemActivatie.class.getResourceAsStream("/resource/win7/slic/bootinst.exe"),
					bootinstTmpPath);
			MyFile.inputStreamToFile(Win7OemActivatie.class.getResourceAsStream("/resource/win7/slic/bootrest.exe"),
					bootrestTmpPath);
			licFile = new File(licTmpPath);
			grldrFile = new File(grldrTmpPath);
			bootinstFile = new File(bootinstTmpPath);
			bootrestFile = new File(bootrestTmpPath);
			if (licFile.exists() && licFile.isFile() && grldrFile.exists() && grldrFile.isFile()
					&& bootinstFile.exists() && bootinstFile.isFile() && bootrestFile.exists()
					&& bootrestFile.isFile()) {
				Win7OemActivatieJPanel.getIstance().consoleLog("已建立缓存文件");
				isTempFileExist = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (isTempFileExist) {
			boolean success = execActivatie(licFile, grldrFile, bootinstFile, bootrestFile);
			if (success) {
				int n = JOptionPane.showConfirmDialog(Win7OemActivatieJPanel.getIstance(), "激活完成，重启后生效，是否现在重启？");
				if (n == JOptionPane.YES_OPTION) {
					try {
						CMDService.execVoid("shutdown -r -t 0");
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			} else {
				JOptionPane.showMessageDialog(Win7OemActivatieJPanel.getIstance(), "激活失败");
			}
		} else {
			JOptionPane.showMessageDialog(Win7OemActivatieJPanel.getIstance(), "无法创建缓存文件");
		}

		Win7OemActivatieJPanel.getIstance().runActivatieButton.setEnabled(true);
		Win7OemActivatieJPanel.getIstance().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	private boolean execActivatie(File licTmpFile, File grldrTmpFile, File bootinstTmpFile, File bootrestTmpFile) {

		boolean flag = false;
		String cmdResult = "";

		try {
			// 寻找系统引导盘
			Win7OemActivatieJPanel.getIstance().consoleLog("系统引导盘符为" + cosBootPath);
			Win7OemActivatieJPanel.getIstance().progressBar.setValue(10);
			// 卸载并安装密钥
			Win7OemActivatieJPanel.getIstance().consoleLog("正在安装密钥...请稍等...");
			cmdResult = CMDService.exec("cmd /c cscript /nologo C:\\Windows\\system32\\slmgr.vbs /upk");
			Thread.sleep(1000);
			cmdResult = CMDService.exec("cmd /c cscript /nologo C:\\Windows\\system32\\slmgr.vbs /ipk " + key);
			Win7OemActivatieJPanel.getIstance().consoleLog("已安装密钥：" + key);
			Win7OemActivatieJPanel.getIstance().progressBar.setValue(25);
			Thread.sleep(1000);
			// 安装license
			cmdResult = CMDService.exec("cmd /c cscript /nologo C:\\Windows\\system32\\slmgr.vbs /ilc "
					+ URLDecoder.decode(licTmpFile.getAbsolutePath(), "UTF-8"));
			Win7OemActivatieJPanel.getIstance().consoleLog("成功地安装了许可证文件");
			Win7OemActivatieJPanel.getIstance().progressBar.setValue(50);
			Thread.sleep(1000);
			// 添加虚拟slic引导
			File grldrFile = new File(cosBootPath + ":\\grldr");
			if (grldrFile.exists() && grldrFile.isFile()) {
				cmdResult = CMDService.exec("cmd /c attrib " + grldrFile.getAbsolutePath() + " -h -s -r");
				Thread.sleep(200);
				cmdResult = CMDService.exec("cmd /c DEL " + grldrFile.getAbsolutePath());
				Thread.sleep(200);
			}
			MyFile.copy(grldrTmpFile.getAbsolutePath(), grldrFile.getAbsolutePath());
			Win7OemActivatieJPanel.getIstance().progressBar.setValue(70);
			Thread.sleep(200);
			cmdResult = CMDService.exec("cmd /c attrib " + grldrFile.getAbsolutePath() + " +h +s +r");
			Win7OemActivatieJPanel.getIstance().consoleLog("已生成slic2.1引导模拟文件");
			Win7OemActivatieJPanel.getIstance().consoleLog("正在写入活动分区...请稍等...");
			Win7OemActivatieJPanel.getIstance().progressBar.setValue(80);
			Thread.sleep(1000);
			// 更新引导
			cmdResult = CMDService.exec("cmd /c " + bootinstTmpFile.getAbsolutePath() + " /nt60 " + cosBootPath + ":");
			Win7OemActivatieJPanel.getIstance().consoleLog(cmdResult.replace("\n", ""));
			Win7OemActivatieJPanel.getIstance().consoleLog("激活完成");
			Win7OemActivatieJPanel.getIstance().progressBar.setValue(95);
			Thread.sleep(1000);
			// 删除缓存文件
			licTmpFile.delete();
			grldrTmpFile.delete();
			bootinstTmpFile.delete();
			bootrestTmpFile.delete();
			Win7OemActivatieJPanel.getIstance().consoleLog("已删除缓存文件");
			Win7OemActivatieJPanel.getIstance().consoleLog("重启计算机以完成激活");
			Win7OemActivatieJPanel.getIstance().progressBar.setValue(100);
			flag = true;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e);
			e.printStackTrace();
		}

		return flag;

	}

}
