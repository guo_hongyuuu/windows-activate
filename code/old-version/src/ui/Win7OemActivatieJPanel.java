package ui;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.TitledBorder;

import common.Static;
import thread.win7.Win7OemActivatie;
import util.MyUitl;

import java.awt.BorderLayout;
import javax.swing.UIManager;
import java.awt.Color;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Win7OemActivatieJPanel extends JFrame {

	private static final long serialVersionUID = 9171456491624210724L;
	private static volatile Win7OemActivatieJPanel instance;

	public JTextArea textArea;
	public JLabel sysLicenseLabel;
	public JButton runActivatieButton;
	public JProgressBar progressBar;

	public Win7OemActivatieJPanel() {

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		/* 系统信息 */
		JPanel sysInfoPanel = new JPanel();
		sysInfoPanel.setBorder(
				new TitledBorder(null, "\u7CFB\u7EDF\u4FE1\u606F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		sysInfoPanel.setBounds(10, 10, 374, 115);
		panel.add(sysInfoPanel);
		sysInfoPanel.setLayout(null);

		// 系统类型
		JLabel sysEditionLabel = new JLabel("");
		sysEditionLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		sysEditionLabel.setBounds(15, 25, 349, 18);
		sysInfoPanel.add(sysEditionLabel);

		// 系统版本
		JLabel sysVersionLabel = new JLabel("");
		sysVersionLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		sysVersionLabel.setBounds(15, 53, 349, 18);
		sysInfoPanel.add(sysVersionLabel);

		// 激活状态
		sysLicenseLabel = new JLabel("\u6FC0\u6D3B\u72B6\u6001\uFF1A\u6B63\u5728\u83B7\u53D6...");
		sysLicenseLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		sysLicenseLabel.setBounds(15, 81, 349, 18);
		sysInfoPanel.add(sysLicenseLabel);
		/* -END- 系统信息 */

		/* 激活选项 */
		JPanel activatieOptionPanel = new JPanel();
		activatieOptionPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"\u6FC0\u6D3B\u9009\u9879\uFF08\u6682\u53EA\u63D0\u4F9B\u8054\u60F3\u8BC1\u4E66\uFF09",
				TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		activatieOptionPanel.setBounds(10, 135, 200, 95);
		panel.add(activatieOptionPanel);
		activatieOptionPanel.setLayout(null);

		JLabel brandTitleLabel = new JLabel("\u54C1\u724C\uFF1A");
		brandTitleLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		brandTitleLabel.setBounds(15, 25, 42, 18);
		activatieOptionPanel.add(brandTitleLabel);

		JComboBox<String> brandComboBox = new JComboBox<String>();
		brandComboBox.setEnabled(false);
		brandComboBox.setFont(new Font("宋体", Font.PLAIN, 13));
		brandComboBox.setFocusable(false);
		brandComboBox.setBounds(67, 22, 118, 24);
		for (String brand : Static.WIN7_OEM_LIC.keySet()) {
			brandComboBox.addItem(brand);
		}
		brandComboBox.setSelectedIndex(0);
		activatieOptionPanel.add(brandComboBox);

		JLabel keyTitleLabel = new JLabel("key\uFF1A");
		keyTitleLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		keyTitleLabel.setBounds(15, 59, 42, 18);
		activatieOptionPanel.add(keyTitleLabel);

		JComboBox<String> keyComboBox = new JComboBox<String>();
		keyComboBox.setEnabled(false);
		keyComboBox.setFont(new Font("宋体", Font.PLAIN, 13));
		keyComboBox.setFocusable(false);
		keyComboBox.setBounds(67, 56, 118, 24);
		activatieOptionPanel.add(keyComboBox);
		/* -END- 激活选项 */

		/* 引导盘符 */
		JPanel activeDiskPanel = new JPanel();
		activeDiskPanel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"),
				"\u5F15\u5BFC\u76D8\u7B26", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		activeDiskPanel.setBounds(220, 135, 164, 95);
		panel.add(activeDiskPanel);
		activeDiskPanel.setLayout(null);

		JLabel activeDiskTitleLabel = new JLabel("");
		activeDiskTitleLabel.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		activeDiskTitleLabel.setBounds(15, 25, 139, 18);
		activeDiskPanel.add(activeDiskTitleLabel);

		JComboBox<String> activeDiskComboBox = new JComboBox<String>();
		activeDiskComboBox.setFont(new Font("宋体", Font.PLAIN, 13));
		activeDiskComboBox.setFocusable(false);
		activeDiskComboBox.setBounds(15, 56, 60, 24);
		activeDiskPanel.add(activeDiskComboBox);
		/* -END- 引导盘符 */

		/* 激活进度 */
		JPanel activatieProcessPanel = new JPanel();
		activatieProcessPanel.setBorder(
				new TitledBorder(null, "\u6FC0\u6D3B\u8FDB\u5EA6", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		activatieProcessPanel.setBounds(10, 240, 374, 170);
		activatieProcessPanel.setLayout(new BorderLayout(0, 0));
		panel.add(activatieProcessPanel);

		JScrollPane scrollPane = new JScrollPane();
		activatieProcessPanel.add(scrollPane, BorderLayout.CENTER);

		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);
		/* --END 激活进度 */

		runActivatieButton = new JButton("\u6FC0\u6D3B");
		runActivatieButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				String lic = Static.WIN7_OEM_LIC.get(brandComboBox.getSelectedItem());
				String key = Static.WIN7_OEM_KEY.get(keyComboBox.getSelectedItem());
				if (lic != null && key != null) {
					new Thread(new Win7OemActivatie(lic, key, (String) activeDiskComboBox.getSelectedItem())).start();
				} else {
					JOptionPane.showMessageDialog(Win7OemActivatieJPanel.getIstance(), "不支持的系统版本");
				}
			}
		});
		runActivatieButton.setFont(new Font("微软雅黑", Font.PLAIN, 13));
		runActivatieButton.setFocusPainted(false);
		runActivatieButton.setBounds(309, 420, 75, 27);
		panel.add(runActivatieButton);

		progressBar = new JProgressBar();
		progressBar.setBounds(10, 421, 289, 26);
		panel.add(progressBar);

		/* 获取系统信息 */
		String sysEdition = null;
		String sysVersion = null;
		try {
			sysEdition = MyUitl.getSysEdition();
			sysVersion = MyUitl.getSysVersion();
			sysEditionLabel.setText("系统类型：" + sysEdition);
			sysVersionLabel.setText("系统版本：" + sysVersion);
		} catch (IOException e) {
			sysEditionLabel.setText("系统类型获取失败");
			sysVersionLabel.setText("系统版本获取失败");
			e.printStackTrace();
		}

		if (sysEdition != null) {
			String verName = Static.WIN7_SYS_VERSION.get(sysEdition);
			for (String key : Static.WIN7_OEM_KEY.keySet()) {
				if (key.contains(verName)) {
					keyComboBox.addItem(key);
				}
			}
			keyComboBox.setSelectedIndex(0);
		}

		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					String sysLicenseValue = MyUitl.getActivatieStatus();
					if (!"".equals(sysLicenseValue)) {
						sysLicenseLabel.setText("激活状态：" + sysLicenseValue);
					} else {
						sysLicenseLabel.setText("激活状态：未找到密钥");
					}
				} catch (Exception e) {
					sysLicenseLabel.setText("激活状态：获取失败");
					e.printStackTrace();
				}
			}
		}).start();

		String[] diskList = new String[] { "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q",
				"R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
		ArrayList<String> bootPaths = new ArrayList<String>();
		for (int i = 0; i < diskList.length; i++) {
			File bootmgrFile = new File(diskList[i] + ":\\bootmgr");
			if (bootmgrFile.exists() && bootmgrFile.isFile()) {
				bootPaths.add(diskList[i]);
			}
		}
		switch (bootPaths.size()) {
		case 0:
			activeDiskTitleLabel.setText("无活动分区");
			activeDiskComboBox.setEnabled(false);
			break;
		case 1:
			activeDiskTitleLabel.setText("只有一个，已自动选择");
			activeDiskComboBox.addItem(bootPaths.get(0));
			activeDiskComboBox.setSelectedItem(0);
			activeDiskComboBox.setEnabled(false);
			break;
		default:
			activeDiskTitleLabel.setText("有多个，需手动选择");
			for (int i = 0; i < bootPaths.size(); i++) {
				activeDiskComboBox.addItem(bootPaths.get(i));
			}
			activeDiskComboBox.setSelectedItem(0);
			break;
		}
		/* -END- 获取系统信息 */

		this.setTitle("Windows 7 OEM\u6FC0\u6D3B\uFF08\u6D4B\u8BD5\u7248\uFF09");
		this.setSize(new Dimension(400, 485));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - this.getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - this.getHeight()) / 2);
		this.setVisible(true);
		this.setResizable(false);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	public void consoleLog(String s) {
		textArea.append(s + "\r\n");
		textArea.setCaretPosition(textArea.getText().length());
	}

	public static Win7OemActivatieJPanel getIstance() {
		if (instance == null) {
			synchronized (Win7OemActivatieJPanel.class) {
				if (instance == null) {
					instance = new Win7OemActivatieJPanel();
				}
			}
		}
		return instance;
	}
}
