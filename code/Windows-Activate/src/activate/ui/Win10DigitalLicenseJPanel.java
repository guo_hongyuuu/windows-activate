package activate.ui;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.border.TitledBorder;

import activate.common.Static;
import activate.exec.win10.Win10DigitalLicenseActivate;
import activate.exec.win10.Win10UninstallKms;
import activate.util.MyUitl;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Win10DigitalLicenseJPanel extends JFrame {

	private static final long serialVersionUID = -3419060749767000232L;
	private static volatile Win10DigitalLicenseJPanel instance;

	public JTextArea textArea;
	public JLabel sysLicenseLabel;
	public JButton runActivatieButton;
	public JButton uninstallKmsButton;
	public JProgressBar progressBar;

	private JLabel sysEditionLabel;
	private JLabel sysVersionLabel;
	private JLabel sysSkuLabel;

	private String SKU;

	public Win10DigitalLicenseJPanel() {
		
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(Win10DigitalLicenseJPanel.class.getResource("/activate/resource/img/win10-key.png")));

		Font yaheiFont13 = new Font("微软雅黑", Font.PLAIN, 13);

		JPanel panel = new JPanel();
		getContentPane().add(panel, BorderLayout.CENTER);

		/* 系统信息 */
		JPanel sysInfoPanel = new JPanel();
		sysInfoPanel.setBorder(
				new TitledBorder(null, "\u7CFB\u7EDF\u4FE1\u606F", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		sysInfoPanel.setLayout(null);

		// 系统类型
		sysEditionLabel = new JLabel("系统类型：正在获取...");
		sysEditionLabel.setFont(yaheiFont13);
		sysEditionLabel.setBounds(15, 25, 349, 18);
		sysInfoPanel.add(sysEditionLabel);

		// 系统版本
		sysVersionLabel = new JLabel("系统版本：正在获取...");
		sysVersionLabel.setFont(yaheiFont13);
		sysVersionLabel.setBounds(15, 53, 349, 18);
		sysInfoPanel.add(sysVersionLabel);

		// 激活状态
		sysLicenseLabel = new JLabel("激活状态：正在获取...");
		sysLicenseLabel.setFont(yaheiFont13);
		sysLicenseLabel.setBounds(15, 81, 349, 18);
		sysInfoPanel.add(sysLicenseLabel);

		// sku
		sysSkuLabel = new JLabel("SKU：正在获取...");
		sysSkuLabel.setBounds(15, 109, 316, 18);
		sysInfoPanel.add(sysSkuLabel);
		sysSkuLabel.setFont(yaheiFont13);
		/* -END- 系统信息 */

		JPanel activatieProcessPanel = new JPanel();
		activatieProcessPanel.setBorder(
				new TitledBorder(null, "\u6FC0\u6D3B\u8FDB\u5EA6", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		activatieProcessPanel.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		activatieProcessPanel.add(scrollPane, BorderLayout.CENTER);

		textArea = new JTextArea();
		textArea.setEditable(false);
		textArea.setLineWrap(true);
		scrollPane.setViewportView(textArea);

		JLabel uninstallKmsLabel = new JLabel(
				"\u82E5\u4E4B\u524D\u4E3Akms\u6FC0\u6D3B\uFF0C\u9700\u5148\u5378\u8F7Dkms");
		uninstallKmsLabel.setFont(yaheiFont13);

		uninstallKmsButton = new JButton("\u5378\u8F7DKMS");
		uninstallKmsButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == 1 && uninstallKmsButton.isEnabled()) {
					new Thread(new Win10UninstallKms()).start();
				}
			}
		});
		uninstallKmsButton.setFont(yaheiFont13);

		runActivatieButton = new JButton("\u6FC0\u6D3B");
		runActivatieButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (e.getButton() == 1 && runActivatieButton.isEnabled()) {
					new Thread(new Win10DigitalLicenseActivate(SKU)).start();
				}
			}
		});
		runActivatieButton.setFont(yaheiFont13);
		runActivatieButton.setFocusPainted(false);
		runActivatieButton.setEnabled(false);

		progressBar = new JProgressBar();

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup().addGap(10)
						.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(activatieProcessPanel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)
								.addComponent(sysInfoPanel, GroupLayout.DEFAULT_SIZE, 370, Short.MAX_VALUE)
								.addGroup(gl_panel.createSequentialGroup()
										.addComponent(uninstallKmsLabel, GroupLayout.DEFAULT_SIZE, 269, Short.MAX_VALUE)
										.addGap(6).addComponent(uninstallKmsButton, GroupLayout.PREFERRED_SIZE, 95,
												GroupLayout.PREFERRED_SIZE))
								.addGroup(gl_panel.createSequentialGroup().addPreferredGap(ComponentPlacement.RELATED)
										.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 289, Short.MAX_VALUE)
										.addPreferredGap(ComponentPlacement.RELATED).addComponent(runActivatieButton,
												GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)))
						.addGap(6)));
		gl_panel.setVerticalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup().addContainerGap()
								.addComponent(sysInfoPanel, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
								.addGap(10)
								.addComponent(activatieProcessPanel, GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
								.addGap(10)
								.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
										.addComponent(uninstallKmsLabel, GroupLayout.PREFERRED_SIZE, 27,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(uninstallKmsButton))
								.addGap(9)
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
										.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 26,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(runActivatieButton, GroupLayout.PREFERRED_SIZE, 27,
												GroupLayout.PREFERRED_SIZE))
								.addGap(16)));
		panel.setLayout(gl_panel);

		/* 获取系统信息 */
		this.getSystemInfo();
		/* -END- 获取系统信息 */

		this.setTitle("Windows 10 \u6570\u5B57\u8BB8\u53EF\u6FC0\u6D3B");
		this.setSize(new Dimension(400, 500));
		this.setMinimumSize(new Dimension(400, 500));
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		this.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width - this.getWidth()) / 2,
				(Toolkit.getDefaultToolkit().getScreenSize().height - this.getHeight()) / 2);
		this.setVisible(true);
		this.setResizable(true);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
	}

	/* 获取系统信息 */
	private void getSystemInfo() {
		SwingWorker<Void, Void> worker = new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() {
				// 系统版本
				try {
					String sysEdition = MyUitl.getSysEdition();
					String sysVersion = MyUitl.getSysVersion();
					sysEditionLabel.setText("系统类型：" + sysEdition);
					sysVersionLabel.setText("系统版本：" + sysVersion);
					String sysSku = Static.WIN10_DIGITAL_LICENSE_SKU.get(sysEdition);
					if (sysSku != null) {
						SKU = sysSku;
						if (Static.WIN10_DIGITAL_LICENSE_KEY.get(sysSku) != null) {
							runActivatieButton.setEnabled(true);
							sysSkuLabel.setText("SKU：" + sysSku);
						} else {
							sysSkuLabel.setText("SKU：" + sysSku + "（该版本暂未支持）");
						}
					}
				} catch (IOException e) {
					sysEditionLabel.setText("系统类型获取失败");
					sysVersionLabel.setText("系统版本获取失败");
					sysSkuLabel.setText("SKU获取失败");
					e.printStackTrace();
				}
				// 激活状态
				try {
					String sysLicenseValue = MyUitl.getActivatieStatus();
					if (!"".equals(sysLicenseValue)) {
						sysLicenseLabel.setText("激活状态：" + sysLicenseValue);
					} else {
						sysLicenseLabel.setText("激活状态：未找到密钥");
					}
				} catch (Exception e) {
					sysLicenseLabel.setText("激活状态：获取失败");
					e.printStackTrace();
				}
				return null;
			}

		};
		worker.execute();
	}

	public void consoleLog(String s) {
		textArea.append(s + "\r\n");
		textArea.setCaretPosition(textArea.getText().length());
	}

	public static Win10DigitalLicenseJPanel getIstance() {
		if (instance == null) {
			synchronized (Win10DigitalLicenseJPanel.class) {
				if (instance == null) {
					instance = new Win10DigitalLicenseJPanel();
				}
			}
		}
		return instance;
	}
}
