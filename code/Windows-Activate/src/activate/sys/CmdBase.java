package activate.sys;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public interface CmdBase {

	/**
	 * @title 普通模式调用CMD
	 * @param command
	 * @return
	 */
	public static String useCMD(String command) throws IOException {
		String line = null;
		StringBuilder sb = new StringBuilder();
		Runtime runtime = Runtime.getRuntime();
		Process process = runtime.exec(command);
		BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
		while ((line = br.readLine()) != null) {
			sb.append(line + "\n");
		}
		br.close();
		process.destroyForcibly();
		return sb.toString();
	}

	public static void useCMDVoid(String command) throws IOException {
		Runtime.getRuntime().exec(command);
	}

	/**
	 * @title 通过nircmd调用CMD
	 * @param command
	 * @return
	 */
	public static String useAdminCMD(String command) {
		String line = null;
		String path = System.getProperty("user.dir") + "\\cmd\\nircmd.exe";
		StringBuilder sb = new StringBuilder();
		Runtime runtime = Runtime.getRuntime();
		try {
			Process process = runtime.exec(path + " elevate " + command);
			BufferedReader br = new BufferedReader(new InputStreamReader(process.getInputStream()));
			while ((line = br.readLine()) != null) {
				System.out.println(123);
				sb.append(line + "\r\n");
			}
			process.destroyForcibly();
			return sb.toString();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
