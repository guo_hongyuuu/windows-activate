package activate.exec.win10;

import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import activate.sys.CmdService;
import activate.ui.Win10DigitalLicenseJPanel;

public class Win10UninstallKms implements Runnable {

	@Override
	public void run() {

		Win10DigitalLicenseJPanel.getIstance().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Win10DigitalLicenseJPanel.getIstance().runActivatieButton.setEnabled(false);
		Win10DigitalLicenseJPanel.getIstance().uninstallKmsButton.setEnabled(false);
		Win10DigitalLicenseJPanel.getIstance().textArea.setText("");

		String cmdResult = "";

		try {
			Win10DigitalLicenseJPanel.getIstance().consoleLog("正在卸载kms...请等待...");
			cmdResult = CmdService.exec("cscript /nologo C:\\Windows\\system32\\slmgr.vbs /upk").replace("\n\n", "");
			Win10DigitalLicenseJPanel.getIstance().consoleLog(cmdResult);
			cmdResult = CmdService.exec("cscript /nologo C:\\Windows\\system32\\slmgr.vbs /ckms").replace("\n\n", "");
			Win10DigitalLicenseJPanel.getIstance().consoleLog(cmdResult);
			cmdResult = CmdService.exec("cscript /nologo C:\\Windows\\system32\\slmgr.vbs /rearm").replace("\n\n", "");
			Win10DigitalLicenseJPanel.getIstance().consoleLog(cmdResult);
			Win10DigitalLicenseJPanel.getIstance().consoleLog("kms卸载完成");
			// 重设激活状态
			String sysLicense = CmdService.exec("cscript /nologo C:\\Windows\\System32\\slmgr.vbs -xpr");
			String sysLicenseValue = sysLicense.substring(sysLicense.indexOf("\n"), sysLicense.length())
					.replace("\n", "").replace(" ", "").replace("。", "").replace(".", "");
			if (!"".equals(sysLicenseValue)) {
				Win10DigitalLicenseJPanel.getIstance().sysLicenseLabel.setText("激活状态：" + sysLicenseValue);
			} else {
				Win10DigitalLicenseJPanel.getIstance().sysLicenseLabel.setText("激活状态：未找到密钥");
			}
			// 提示重启
			int n = JOptionPane.showConfirmDialog(Win10DigitalLicenseJPanel.getIstance(), "kms卸载完成，需重启电脑，是否重启电脑？");
			if (n == JOptionPane.YES_OPTION) {
				CmdService.execVoid("shutdown -r -t 0");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		Win10DigitalLicenseJPanel.getIstance().runActivatieButton.setEnabled(true);
		Win10DigitalLicenseJPanel.getIstance().uninstallKmsButton.setEnabled(true);
		Win10DigitalLicenseJPanel.getIstance().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

}
