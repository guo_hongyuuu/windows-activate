package activate.exec.win10;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import activate.common.Static;
import activate.sys.CmdService;
import activate.ui.Win10DigitalLicenseJPanel;
import activate.util.MyFile;
import activate.util.MyUitl;

public class Win10DigitalLicenseActivate implements Runnable {

	private String SKU;

	public Win10DigitalLicenseActivate(String SKU) {
		this.SKU = SKU;
	}

	@Override
	public void run() {

		Win10DigitalLicenseJPanel.getIstance().setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Win10DigitalLicenseJPanel.getIstance().runActivatieButton.setEnabled(false);
		Win10DigitalLicenseJPanel.getIstance().uninstallKmsButton.setEnabled(false);
		Win10DigitalLicenseJPanel.getIstance().textArea.setText("");
		Win10DigitalLicenseJPanel.getIstance().progressBar.setValue(0);

		// 复制相关文件至系统temp目录下
		boolean isTempFileExist = false;
		String gsExeTmpPath = null;
		String slcDllTmpPath = null;
		String slmgrBatTmpPath = null;
		File gsExeFile = null;
		File slcDllFile = null;
		try {
			gsExeTmpPath = URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "gatherosstate.exe";
			slcDllTmpPath = URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "slc.dll";
			slmgrBatTmpPath = URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "slmgr.bat";
			MyFile.inputStreamToFile(
					Win10DigitalLicenseActivate.class.getResourceAsStream("/activate/resource/win10/gatherosstate.exe"),
					gsExeTmpPath);
			MyFile.inputStreamToFile(Win10DigitalLicenseActivate.class.getResourceAsStream("/activate/resource/win10/slc.dll"),
					slcDllTmpPath);
			MyFile.inputStreamToFile(
					Win10DigitalLicenseActivate.class.getResourceAsStream("/activate/resource/win10/slmgr.bat"),
					slmgrBatTmpPath);
			gsExeFile = new File(gsExeTmpPath);
			slcDllFile = new File(slcDllTmpPath);
			if (gsExeFile.exists() && gsExeFile.isFile() && slcDllFile.exists() && slcDllFile.isFile()) {
				Win10DigitalLicenseJPanel.getIstance().consoleLog("已建立缓存文件");
				isTempFileExist = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (isTempFileExist) {
			File ticketFile = null;
			try {
				ticketFile = new File(URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8") + "GenuineTicket.xml");
				if (ticketFile.exists() && ticketFile.isFile()) {
					ticketFile.delete();
				}
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
			// 执行激活方法
			boolean success = execActivatie(gsExeFile, slcDllFile, ticketFile);
			if (success) {
				File slmgrFile = new File(slmgrBatTmpPath);
				int n = JOptionPane.showConfirmDialog(Win10DigitalLicenseJPanel.getIstance(), "激活完成，是否查看激活信息？");
				if (n == JOptionPane.YES_OPTION) {
					if (slmgrFile.exists() && slmgrFile.isFile()) {
						try {
							@SuppressWarnings("unused")
							String w = CmdService.exec("cmd /c " + slmgrFile.getAbsolutePath());
						} catch (IOException e) {
							e.printStackTrace();
						}
						slmgrFile.delete();
					}
				} else {
					slmgrFile.delete();
				}
			} else {
				JOptionPane.showMessageDialog(Win10DigitalLicenseJPanel.getIstance(), "激活失败");
			}
		} else {
			JOptionPane.showMessageDialog(Win10DigitalLicenseJPanel.getIstance(), "无法创建缓存文件");
		}

		Win10DigitalLicenseJPanel.getIstance().runActivatieButton.setEnabled(true);
		Win10DigitalLicenseJPanel.getIstance().uninstallKmsButton.setEnabled(true);
		Win10DigitalLicenseJPanel.getIstance().setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	}

	private boolean execActivatie(File gsExeTmpFile, File slcDllTmpFile, File ticketTmpFile) {

		boolean flag = false;

		String key = Static.WIN10_DIGITAL_LICENSE_KEY.get(SKU);
		String cmdResult = "";

		try {
			// 添加注册表项
			cmdResult = CmdService
					.exec("cmd /c reg add \"HKLM\\SYSTEM\\Tokens\" /v \"Channel\" /t REG_SZ /d \"Retail\" /f");
			cmdResult = CmdService
					.exec("cmd /c reg add \"HKLM\\SYSTEM\\Tokens\\Kernel\" /v \"Kernel-ProductInfo\" /t REG_DWORD /d "
							+ SKU + " /f");
			cmdResult = CmdService.exec(
					"cmd /c reg add \"HKLM\\SYSTEM\\Tokens\\Kernel\" /v \"Security-SPP-GenuineLocalStatus\" /t REG_DWORD /d 1 /f");
			Win10DigitalLicenseJPanel.getIstance().consoleLog("已添加注册表项");
			Win10DigitalLicenseJPanel.getIstance().progressBar.setValue(20);
			Thread.sleep(1000);
			// 卸载并安装密钥
			Win10DigitalLicenseJPanel.getIstance().consoleLog("正在安装密钥...请稍等...");
			cmdResult = CmdService.exec("cmd /c cscript /nologo C:\\Windows\\system32\\slmgr.vbs /upk");
			Thread.sleep(1000);
			cmdResult = CmdService.exec("cmd /c cscript /nologo C:\\Windows\\system32\\slmgr.vbs /ipk " + key);
			Win10DigitalLicenseJPanel.getIstance().consoleLog("已安装密钥：" + key);
			Win10DigitalLicenseJPanel.getIstance().progressBar.setValue(40);
			Thread.sleep(1000);
			// 生成门票
			Win10DigitalLicenseJPanel.getIstance().consoleLog("正在生成门票...请稍等...");
			cmdResult = CmdService.exec("cmd /c " + gsExeTmpFile.getAbsolutePath());
			while (true) {
				if (ticketTmpFile.exists() && ticketTmpFile.isFile()) {
					break;
				}
				Thread.sleep(3000);
			}
			Win10DigitalLicenseJPanel.getIstance().consoleLog("已生成门票");
			Win10DigitalLicenseJPanel.getIstance().consoleLog("正在应用门票...请稍等...");
			Win10DigitalLicenseJPanel.getIstance().progressBar.setValue(70);
			Thread.sleep(1000);
			// 应用门票
			cmdResult = CmdService.exec("cmd /c C:\\Windows\\system32\\ClipUp.exe -v -o -altto "
					+ URLDecoder.decode(Static.SYS_TEMP_FOLDER, "UTF-8"));
			Win10DigitalLicenseJPanel.getIstance().consoleLog(cmdResult.replace("\n", ""));
			Win10DigitalLicenseJPanel.getIstance().consoleLog("已应用门票");
			Win10DigitalLicenseJPanel.getIstance().consoleLog("正在激活...请稍等...");
			Win10DigitalLicenseJPanel.getIstance().progressBar.setValue(80);
			Thread.sleep(2000);
			// 激活系统
			cmdResult = CmdService.exec("cmd /c cscript /nologo C:\\Windows\\system32\\slmgr.vbs -ato");
			Win10DigitalLicenseJPanel.getIstance().consoleLog(cmdResult.replace("\n", ""));
			boolean isSuccess = false;
			if (cmdResult.contains("0x80070422")) {
				Win10DigitalLicenseJPanel.getIstance().consoleLog("Windows Update服务被禁用，请开启该服务");
				Win10DigitalLicenseJPanel.getIstance().consoleLog("激活失败");
			} else if (cmdResult.contains("0xC004C003")) {
				Win10DigitalLicenseJPanel.getIstance().consoleLog("可能情况1、无法连接Windows激活服务器，请检查网络或稍后再试");
				Win10DigitalLicenseJPanel.getIstance().consoleLog("可能情况2、你的系统（或电脑）不支持破解数字权利激活");
				Win10DigitalLicenseJPanel.getIstance().consoleLog("激活失败");
			} else if (cmdResult.contains("0x80072EE7")) {
				Win10DigitalLicenseJPanel.getIstance().consoleLog("无法连接Windows激活服务器，请检查网络或稍后再试");
				Win10DigitalLicenseJPanel.getIstance().consoleLog("激活失败");
			} else {
				isSuccess = true;
				Win10DigitalLicenseJPanel.getIstance().consoleLog("激活完成");
			}
			if (!isSuccess) {
				Win10DigitalLicenseJPanel.getIstance().consoleLog("若出现问题，请按照提示检查相关错误，或检查您的系统是否为msdn原版系统");
			}
			Win10DigitalLicenseJPanel.getIstance().progressBar.setValue(95);
			Thread.sleep(1000);
			// 删除缓存
			ticketTmpFile.delete();
			gsExeTmpFile.delete();
			slcDllTmpFile.delete();
			// 删除注册表项
			cmdResult = CmdService.exec("cmd /c reg delete \"HKLM\\SYSTEM\\Tokens\" /f");
			Win10DigitalLicenseJPanel.getIstance().consoleLog("已删除缓存");
			Win10DigitalLicenseJPanel.getIstance().progressBar.setValue(100);
			// 重设激活状态
			String sysLicenseValue = MyUitl.getActivatieStatus();
			if (!"".equals(sysLicenseValue)) {
				Win10DigitalLicenseJPanel.getIstance().sysLicenseLabel.setText("激活状态：" + sysLicenseValue);
			} else {
				Win10DigitalLicenseJPanel.getIstance().sysLicenseLabel.setText("激活状态：未找到密钥");
			}
			flag = true;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return flag;

	}

}
