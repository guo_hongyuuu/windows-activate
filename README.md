# Windows激活工具

#### 项目介绍
Java开发的win7oem、win10/11数字权利激活工具，代码公开无毒无后台，Windows Defender不报毒。可以直接支持Windows11。

#### 使用说明
1. 下载“Windows激活工具build.7z”，解压运行即可，若是32位系统请下载旧版

#### 代码说明
1. 开发工具eclipse，window builder
2. 环境openjdk13，使用jlink打包精简jre，支持Windows高分屏缩放下显示
3. 项目编码为GBK（Windows默认为GBK，使用UTF-8读取cmd执行结果转码麻烦）
4. 开发过程中需要通过管理员身份打开eclipse

#### 参考教程
1. http://bbs.wuyou.net/forum.php?mod=viewthread&tid=409326&extra=&page=1
2. https://www.zhihu.com/question/25695388
3. https://club.kdslife.com/t_3783586_0_0.html

#### 注意
1. 安装系统请使用msdn官方镜像，非原版系统例如ghost无法激活作者不会去处理相关错误问题
2. Windows7激活仅支持旗舰版，Windows10不支持LTSC
3. 新版基于jdk13开发仅支持64位，旧版支持32位但不再更新
4. Windows7激活无需联网，无法激活GPT磁盘安装的系统，请使用MBR
5. Windows10数字权利激活过程需联网


#### 开发者的自用电脑，数字激活专业版
![开发者的自用电脑](https://images.gitee.com/uploads/images/2020/0315/182609_82ec928b_1403243.png "WJ8F`DT]S$T7UT(XHZ4@%(Q.png")

#### Win10以及旧版和新版的截图
![旧版和新版的截图](https://images.gitee.com/uploads/images/2020/0205/155121_0c26a32d_1403243.png "L$9C6`MYTA`3YA{DDN39`AM.png")

#### Win7截图
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/203902_2f693fc2_1403243.jpeg "WHW@IZAS5()9~K1TV4TJA_0_看图王.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/203946_d87ffa24_1403243.jpeg "%R$5UZI@VB_0$]CFC{DV@%2_看图王.jpg")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0207/204017_9fa51349_1403243.jpeg "1RY9F}NK(17R[4PBVHN45NU_看图王.jpg")